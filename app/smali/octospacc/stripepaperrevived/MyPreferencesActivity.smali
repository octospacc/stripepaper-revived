.class public Loctospacc/stripepaperrevived/MyPreferencesActivity;
.super Landroid/preference/PreferenceActivity;
.source "MyPreferencesActivity.java"


# instance fields
.field spChanged:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 11
    new-instance v0, Loctospacc/stripepaperrevived/MyPreferencesActivity$1;

    invoke-direct {v0, p0}, Loctospacc/stripepaperrevived/MyPreferencesActivity$1;-><init>(Loctospacc/stripepaperrevived/MyPreferencesActivity;)V

    iput-object v0, p0, Loctospacc/stripepaperrevived/MyPreferencesActivity;->spChanged:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const/high16 v0, 0x7f040000

    invoke-virtual {p0, v0}, Loctospacc/stripepaperrevived/MyPreferencesActivity;->addPreferencesFromResource(I)V

    .line 25
    invoke-virtual {p0}, Loctospacc/stripepaperrevived/MyPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Loctospacc/stripepaperrevived/MyPreferencesActivity;->spChanged:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 26
    return-void
.end method
