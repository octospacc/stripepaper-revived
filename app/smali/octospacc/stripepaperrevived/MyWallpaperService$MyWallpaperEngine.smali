.class public Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;
.super Landroid/service/wallpaper/WallpaperService$Engine;
.source "MyWallpaperService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Loctospacc/stripepaperrevived/MyWallpaperService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyWallpaperEngine"
.end annotation


# instance fields
.field private colors:Landroid/content/res/TypedArray;

.field private currentTheme:Ljava/lang/String;

.field private final mDraw:Ljava/lang/Runnable;

.field private mVisible:Z

.field private maxNumber:I

.field private paint:Landroid/graphics/Paint;

.field private prefs:Landroid/content/SharedPreferences;

.field private speed:F

.field private speedRange:F

.field private stripes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Loctospacc/stripepaperrevived/Stripe;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Loctospacc/stripepaperrevived/MyWallpaperService;

.field private width:F

.field private widthRange:F


# direct methods
.method public constructor <init>(Loctospacc/stripepaperrevived/MyWallpaperService;)V
    .locals 2

    .prologue
    .line 50
    iput-object p1, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->this$0:Loctospacc/stripepaperrevived/MyWallpaperService;

    invoke-direct {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;-><init>(Landroid/service/wallpaper/WallpaperService;)V

    .line 34
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->paint:Landroid/graphics/Paint;

    .line 43
    const/16 v0, 0xc8

    iput v0, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->maxNumber:I

    .line 44
    new-instance v0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine$1;

    invoke-direct {v0, p0}, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine$1;-><init>(Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;)V

    iput-object v0, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->mDraw:Ljava/lang/Runnable;

    .line 51
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->prefs:Landroid/content/SharedPreferences;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->stripes:Ljava/util/List;

    .line 53
    iget-object v0, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 54
    invoke-direct {p0}, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->checkForChanges()V

    .line 55
    invoke-direct {p0}, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->refreshFrame()V

    .line 56
    return-void
.end method

.method static synthetic access$000(Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;)V
    .locals 0
    .param p0, "x0"    # Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;

    .prologue
    .line 32
    invoke-direct {p0}, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->drawFrame()V

    return-void
.end method

.method private checkForChanges()V
    .locals 12

    .prologue
    const/high16 v11, 0x42480000    # 50.0f

    .line 177
    const/4 v0, 0x0

    .line 178
    .local v0, "change":Z
    iget-object v7, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->prefs:Landroid/content/SharedPreferences;

    const-string v8, "theme"

    const-string v9, "algeria"

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 179
    .local v4, "theme":Ljava/lang/String;
    iget-object v7, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->currentTheme:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 180
    iput-object v4, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->currentTheme:Ljava/lang/String;

    .line 181
    iget-object v7, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->this$0:Loctospacc/stripepaperrevived/MyWallpaperService;

    invoke-virtual {v7}, Loctospacc/stripepaperrevived/MyWallpaperService;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-object v8, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->currentTheme:Ljava/lang/String;

    const-string v9, "array"

    iget-object v10, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->this$0:Loctospacc/stripepaperrevived/MyWallpaperService;

    invoke-virtual {v10}, Loctospacc/stripepaperrevived/MyWallpaperService;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 182
    .local v1, "id":I
    iget-object v7, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->this$0:Loctospacc/stripepaperrevived/MyWallpaperService;

    invoke-virtual {v7}, Loctospacc/stripepaperrevived/MyWallpaperService;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v7

    iput-object v7, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->colors:Landroid/content/res/TypedArray;

    .line 183
    const/4 v0, 0x1

    .line 185
    .end local v1    # "id":I
    :cond_0
    iget-object v7, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->prefs:Landroid/content/SharedPreferences;

    const-string v8, "width"

    const-string v9, "50"

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    .line 186
    .local v5, "w":F
    iget v7, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->width:F

    cmpl-float v7, v5, v7

    if-eqz v7, :cond_1

    .line 187
    iput v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->width:F

    .line 188
    const/4 v0, 0x1

    .line 190
    :cond_1
    iget-object v7, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->prefs:Landroid/content/SharedPreferences;

    const-string v8, "widthRange"

    const-string v9, "20"

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    .line 191
    .local v6, "wR":F
    iget v7, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->widthRange:F

    cmpl-float v7, v6, v7

    if-eqz v7, :cond_2

    .line 192
    iput v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->widthRange:F

    .line 193
    const/4 v0, 0x1

    .line 195
    :cond_2
    iget-object v7, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->prefs:Landroid/content/SharedPreferences;

    const-string v8, "speed"

    const-string v9, "1"

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    div-float v2, v7, v11

    .line 196
    .local v2, "s":F
    iget v7, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->speed:F

    cmpl-float v7, v2, v7

    if-eqz v7, :cond_3

    .line 197
    iput v2, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->speed:F

    .line 198
    const/4 v0, 0x1

    .line 200
    :cond_3
    iget-object v7, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->prefs:Landroid/content/SharedPreferences;

    const-string v8, "speedRange"

    const-string v9, "2"

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    div-float v3, v7, v11

    .line 201
    .local v3, "sR":F
    iget v7, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->speedRange:F

    cmpl-float v7, v3, v7

    if-eqz v7, :cond_4

    .line 202
    iput v3, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->speedRange:F

    .line 203
    const/4 v0, 0x1

    .line 206
    :cond_4
    if-eqz v0, :cond_5

    .line 207
    invoke-direct {p0}, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->refreshFrame()V

    .line 209
    :cond_5
    return-void
.end method

.method private drawFrame()V
    .locals 14

    .prologue
    const/high16 v11, -0x40800000    # -1.0f

    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    const/4 v10, 0x0

    .line 112
    invoke-virtual {p0}, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->getSurfaceHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    .line 113
    .local v1, "holder":Landroid/view/SurfaceHolder;
    const/4 v0, 0x0

    .line 115
    .local v0, "c":Landroid/graphics/Canvas;
    :try_start_0
    invoke-interface {v1}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_8

    .line 117
    invoke-direct {p0}, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->checkForChanges()V

    .line 118
    iget-object v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->stripes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Loctospacc/stripepaperrevived/Stripe;

    .line 119
    .local v3, "point":Loctospacc/stripepaperrevived/Stripe;
    iget v5, v3, Loctospacc/stripepaperrevived/Stripe;->x:F

    iget v6, v3, Loctospacc/stripepaperrevived/Stripe;->speed:F

    add-float/2addr v5, v6

    iput v5, v3, Loctospacc/stripepaperrevived/Stripe;->x:F

    .line 120
    iget v5, v3, Loctospacc/stripepaperrevived/Stripe;->x:F

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v6

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_4

    .line 121
    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v5

    int-to-float v5, v5

    iput v5, v3, Loctospacc/stripepaperrevived/Stripe;->x:F

    .line 122
    iget v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->speed:F

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    cmpl-double v5, v8, v12

    if-lez v5, :cond_3

    iget v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->speedRange:F

    neg-float v5, v5

    :goto_1
    add-float/2addr v5, v6

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Double;->floatValue()F

    move-result v4

    .line 123
    .local v4, "s":F
    cmpl-float v5, v4, v10

    if-lez v5, :cond_1

    .line 124
    mul-float/2addr v4, v11

    .line 126
    :cond_1
    iput v4, v3, Loctospacc/stripepaperrevived/Stripe;->speed:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 139
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "point":Loctospacc/stripepaperrevived/Stripe;
    .end local v4    # "s":F
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_2

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_2
    throw v5

    .line 122
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "point":Loctospacc/stripepaperrevived/Stripe;
    :cond_3
    :try_start_1
    iget v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->speedRange:F

    goto :goto_1

    .line 127
    :cond_4
    iget v5, v3, Loctospacc/stripepaperrevived/Stripe;->x:F

    cmpg-float v5, v5, v10

    if-gez v5, :cond_0

    .line 128
    const/4 v5, 0x0

    iput v5, v3, Loctospacc/stripepaperrevived/Stripe;->x:F

    .line 129
    iget v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->speed:F

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    cmpl-double v5, v8, v12

    if-lez v5, :cond_6

    iget v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->speedRange:F

    neg-float v5, v5

    :goto_2
    add-float/2addr v5, v6

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Double;->floatValue()F

    move-result v4

    .line 130
    .restart local v4    # "s":F
    cmpg-float v5, v4, v10

    if-gez v5, :cond_5

    .line 131
    add-float/2addr v4, v11

    .line 133
    :cond_5
    iput v4, v3, Loctospacc/stripepaperrevived/Stripe;->speed:F

    goto :goto_0

    .line 129
    .end local v4    # "s":F
    :cond_6
    iget v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->speedRange:F

    goto :goto_2

    .line 136
    .end local v3    # "point":Loctospacc/stripepaperrevived/Stripe;
    :cond_7
    iget-object v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->stripes:Ljava/util/List;

    invoke-direct {p0, v0, v5}, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->drawStripes(Landroid/graphics/Canvas;Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 139
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_8
    if-eqz v0, :cond_9

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 143
    :cond_9
    iget-object v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->this$0:Loctospacc/stripepaperrevived/MyWallpaperService;

    invoke-static {v5}, Loctospacc/stripepaperrevived/MyWallpaperService;->access$100(Loctospacc/stripepaperrevived/MyWallpaperService;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->mDraw:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 144
    iget-boolean v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->mVisible:Z

    if-eqz v5, :cond_a

    .line 145
    iget-object v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->this$0:Loctospacc/stripepaperrevived/MyWallpaperService;

    invoke-static {v5}, Loctospacc/stripepaperrevived/MyWallpaperService;->access$100(Loctospacc/stripepaperrevived/MyWallpaperService;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->mDraw:Ljava/lang/Runnable;

    const-wide/16 v8, 0x5

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 147
    :cond_a
    return-void
.end method

.method private drawStripes(Landroid/graphics/Canvas;Ljava/util/List;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Ljava/util/List",
            "<",
            "Loctospacc/stripepaperrevived/Stripe;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 163
    .local p2, "stripes":Ljava/util/List;, "Ljava/util/List<Loctospacc/stripepaperrevived/Stripe;>;"
    const/high16 v0, -0x1000000

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 164
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Loctospacc/stripepaperrevived/Stripe;

    .line 165
    .local v7, "stripe":Loctospacc/stripepaperrevived/Stripe;
    iget-object v0, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->paint:Landroid/graphics/Paint;

    iget v1, v7, Loctospacc/stripepaperrevived/Stripe;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 166
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v4, v0

    .line 167
    .local v4, "screenHeight":F
    iget v0, v7, Loctospacc/stripepaperrevived/Stripe;->x:F

    iget v1, v7, Loctospacc/stripepaperrevived/Stripe;->size:F

    sub-float v1, v0, v1

    const/4 v2, 0x0

    iget v0, v7, Loctospacc/stripepaperrevived/Stripe;->x:F

    iget v3, v7, Loctospacc/stripepaperrevived/Stripe;->size:F

    add-float/2addr v3, v0

    iget-object v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 169
    .end local v4    # "screenHeight":F
    .end local v7    # "stripe":Loctospacc/stripepaperrevived/Stripe;
    :cond_0
    return-void
.end method

.method private getRandomColor()I
    .locals 6

    .prologue
    .line 172
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    iget-object v4, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->colors:Landroid/content/res/TypedArray;

    invoke-virtual {v4}, Landroid/content/res/TypedArray;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-double v4, v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-double v0, v2

    .line 173
    .local v0, "val":D
    iget-object v2, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->colors:Landroid/content/res/TypedArray;

    double-to-int v3, v0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    return v2
.end method

.method private refreshFrame()V
    .locals 12

    .prologue
    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    .line 150
    iget-object v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->stripes:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 151
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->maxNumber:I

    if-ge v1, v6, :cond_3

    .line 152
    iget-object v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->this$0:Loctospacc/stripepaperrevived/MyWallpaperService;

    invoke-virtual {v6}, Loctospacc/stripepaperrevived/MyWallpaperService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "window"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    .line 153
    .local v4, "wm":Landroid/view/WindowManager;
    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 154
    .local v0, "display":Landroid/view/Display;
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v6

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v8

    int-to-double v8, v8

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Double;->floatValue()F

    move-result v5

    .line 155
    .local v5, "x":F
    iget v7, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->speed:F

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    cmpl-double v6, v8, v10

    if-lez v6, :cond_0

    iget v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->speedRange:F

    neg-float v6, v6

    :goto_1
    add-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Double;->floatValue()F

    move-result v2

    .line 156
    .local v2, "s":F
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v6

    cmpl-double v6, v6, v10

    if-lez v6, :cond_1

    const/4 v6, -0x1

    :goto_2
    int-to-float v6, v6

    mul-float/2addr v2, v6

    .line 157
    iget v7, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->width:F

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    cmpl-double v6, v8, v10

    if-lez v6, :cond_2

    iget v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->widthRange:F

    neg-float v6, v6

    :goto_3
    add-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Double;->floatValue()F

    move-result v3

    .line 158
    .local v3, "w":F
    iget-object v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->stripes:Ljava/util/List;

    new-instance v7, Loctospacc/stripepaperrevived/Stripe;

    invoke-direct {p0}, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->getRandomColor()I

    move-result v8

    invoke-direct {v7, v3, v5, v8, v2}, Loctospacc/stripepaperrevived/Stripe;-><init>(FFIF)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 155
    .end local v2    # "s":F
    .end local v3    # "w":F
    :cond_0
    iget v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->speedRange:F

    goto :goto_1

    .line 156
    .restart local v2    # "s":F
    :cond_1
    const/4 v6, 0x1

    goto :goto_2

    .line 157
    :cond_2
    iget v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->widthRange:F

    goto :goto_3

    .line 160
    .end local v0    # "display":Landroid/view/Display;
    .end local v2    # "s":F
    .end local v4    # "wm":Landroid/view/WindowManager;
    .end local v5    # "x":F
    :cond_3
    return-void
.end method


# virtual methods
.method public onDestroy()V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->onDestroy()V

    .line 61
    iget-object v0, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->this$0:Loctospacc/stripepaperrevived/MyWallpaperService;

    invoke-static {v0}, Loctospacc/stripepaperrevived/MyWallpaperService;->access$100(Loctospacc/stripepaperrevived/MyWallpaperService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->mDraw:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 62
    return-void
.end method

.method public onSurfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 77
    invoke-super {p0, p1, p2, p3, p4}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceChanged(Landroid/view/SurfaceHolder;III)V

    .line 78
    return-void
.end method

.method public onSurfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceCreated(Landroid/view/SurfaceHolder;)V

    .line 83
    return-void
.end method

.method public onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 87
    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->mVisible:Z

    .line 89
    iget-object v0, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->this$0:Loctospacc/stripepaperrevived/MyWallpaperService;

    invoke-static {v0}, Loctospacc/stripepaperrevived/MyWallpaperService;->access$100(Loctospacc/stripepaperrevived/MyWallpaperService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->mDraw:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 90
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    .line 94
    iget-object v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->prefs:Landroid/content/SharedPreferences;

    const-string v7, "touch"

    invoke-interface {v5, v7, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 96
    .local v2, "touchEnabled":Z
    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_1

    .line 97
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 98
    .local v4, "x":F
    iget v7, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->speed:F

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    cmpl-double v5, v8, v10

    if-lez v5, :cond_2

    iget v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->speedRange:F

    neg-float v5, v5

    :goto_0
    add-float/2addr v5, v7

    float-to-double v8, v5

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Double;->floatValue()F

    move-result v1

    .line 99
    .local v1, "s":F
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    cmpl-double v5, v8, v10

    if-lez v5, :cond_3

    const/4 v5, -0x1

    :goto_1
    int-to-float v5, v5

    mul-float/2addr v1, v5

    .line 100
    iget v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->width:F

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    cmpl-double v5, v8, v10

    if-lez v5, :cond_4

    iget v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->widthRange:F

    neg-float v5, v5

    :goto_2
    add-float/2addr v5, v6

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Double;->floatValue()F

    move-result v3

    .line 101
    .local v3, "w":F
    iget-object v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->stripes:Ljava/util/List;

    new-instance v6, Loctospacc/stripepaperrevived/Stripe;

    invoke-direct {p0}, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->getRandomColor()I

    move-result v7

    invoke-direct {v6, v3, v4, v7, v1}, Loctospacc/stripepaperrevived/Stripe;-><init>(FFIF)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    iget-object v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->stripes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    iget v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->maxNumber:I

    if-lt v5, v6, :cond_0

    .line 104
    iget-object v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->stripes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    iget v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->maxNumber:I

    sub-int v0, v5, v6

    .line 105
    .local v0, "difference":I
    iget-object v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->stripes:Ljava/util/List;

    iget-object v6, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->stripes:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {v5, v0, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->stripes:Ljava/util/List;

    .line 107
    .end local v0    # "difference":I
    :cond_0
    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 109
    .end local v1    # "s":F
    .end local v3    # "w":F
    .end local v4    # "x":F
    :cond_1
    return-void

    .line 98
    .restart local v4    # "x":F
    :cond_2
    iget v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->speedRange:F

    goto :goto_0

    .restart local v1    # "s":F
    :cond_3
    move v5, v6

    .line 99
    goto :goto_1

    .line 100
    :cond_4
    iget v5, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->widthRange:F

    goto :goto_2
.end method

.method public onVisibilityChanged(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->mVisible:Z

    .line 67
    if-eqz p1, :cond_0

    .line 68
    invoke-direct {p0}, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->checkForChanges()V

    .line 69
    invoke-direct {p0}, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->drawFrame()V

    .line 73
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->this$0:Loctospacc/stripepaperrevived/MyWallpaperService;

    invoke-static {v0}, Loctospacc/stripepaperrevived/MyWallpaperService;->access$100(Loctospacc/stripepaperrevived/MyWallpaperService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Loctospacc/stripepaperrevived/MyWallpaperService$MyWallpaperEngine;->mDraw:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
