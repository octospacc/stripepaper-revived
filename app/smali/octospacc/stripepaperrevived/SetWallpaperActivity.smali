.class public Loctospacc/stripepaperrevived/SetWallpaperActivity;
.super Landroid/app/Activity;
.source "SetWallpaperActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Loctospacc/stripepaperrevived/SetWallpaperActivity;)V
    .locals 0
    .param p0, "x0"    # Loctospacc/stripepaperrevived/SetWallpaperActivity;

    .prologue
    .line 14
    invoke-direct {p0}, Loctospacc/stripepaperrevived/SetWallpaperActivity;->resetToDefaults()V

    return-void
.end method

.method private resetToDefaults()V
    .locals 2

    .prologue
    .line 47
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 48
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 49
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 17
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 18
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Loctospacc/stripepaperrevived/SetWallpaperActivity;->setContentView(I)V

    .line 19
    return-void
.end method

.method public onDefaultClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 28
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 29
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f060004

    invoke-virtual {p0, v1}, Loctospacc/stripepaperrevived/SetWallpaperActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 30
    const v1, 0x7f060001

    invoke-virtual {p0, v1}, Loctospacc/stripepaperrevived/SetWallpaperActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 31
    const v1, 0x1040013

    new-instance v2, Loctospacc/stripepaperrevived/SetWallpaperActivity$1;

    invoke-direct {v2, p0}, Loctospacc/stripepaperrevived/SetWallpaperActivity$1;-><init>(Loctospacc/stripepaperrevived/SetWallpaperActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 37
    const v1, 0x1040009

    new-instance v2, Loctospacc/stripepaperrevived/SetWallpaperActivity$2;

    invoke-direct {v2, p0}, Loctospacc/stripepaperrevived/SetWallpaperActivity$2;-><init>(Loctospacc/stripepaperrevived/SetWallpaperActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 43
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 44
    return-void
.end method

.method public onPreferenceClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 22
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.service.wallpaper.CHANGE_LIVE_WALLPAPER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 23
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.service.wallpaper.extra.LIVE_WALLPAPER_COMPONENT"

    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Loctospacc/stripepaperrevived/MyWallpaperService;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 24
    invoke-virtual {p0, v0}, Loctospacc/stripepaperrevived/SetWallpaperActivity;->startActivity(Landroid/content/Intent;)V

    .line 25
    return-void
.end method
