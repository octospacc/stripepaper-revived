# Stripepaper Revived
Fork of [tjmolinski's Stripepaper](https://github.com/tjmolinski/stripepaper), a nice palette-based live wallpaper app 

## What's this?
A live wallpaper where nicely colored stripes cover the screen.
![](https://gitlab.com/octospacc/stripepaper-revived/-/raw/master/Screenshot.png)

#### What does this fork change in practice?
It just adds new palettes (17 for now, on top of the 22 the original app came with), since the original app only makes you use a list of built-in ones instead of letting you create your own.

Credits for the new palettes used are in the same xml file where they are declared, while credits for the original ones are in the original source files (which is backed up here: [original/tjmolinski-source-master.zip](https://gitlab.com/octospacc/stripepaper-revived/-/blob/master/original/tjmolinski-source-master.zip))

##### Adding new palettes
Palettes are declared inside the following files:
- app/res/values/arrays.xml
- app/res/values/public.xml
- app/smali/octospacc/stripepaperrevived/R$array.smali

I encourage to edit these files and make a pull request here, if you want to see a new palette you found or created in the app.

### Downloading the precompiled APK
You can find it here: [dist/StripepaperRevived.apk](https://gitlab.com/octospacc/stripepaper-revived/-/raw/master/dist/StripepaperRevived.apk?inline=false)

### Building from source
- Clone the repo locally / Download the files
- Get yourself [Apktool](https://ibotpeaches.github.io/Apktool) 
- In the "app" folder, do `apktool b .`

To install the APK after building with Apktool, you must also sign it manually. See the [official Android documentation](https://developer.android.com/studio/command-line/apksigner).

#### Why is the source code the decompiled apk?
Because the original app source is so old that I couldn't get it to properly compile. So at the end I just decompiled the original APK file using Apktool, and made edits to the .xml and .smali files.

### Known issues
The app lags. It really lags badly and makes the home screen lag, and this problem comes from the original version, not from my fork. I have no idea how to fix.

A good thing you can do is to set the "Speed Range" setting to 0, "Speed" to 1, and disable "Enable Touch". This makes everything run smoother.

You can also set "Speed" to 0 but doing that will make the wallpaper completely static and thus not really fun.